package ar.edu.unlu.poo.biblioca;

import ar.edu.unlu.poo.biblioca.model.Libro;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Libro li = new Libro("0001", "Mi primer libro");
        System.out.println(li);
        li.setAutor("Autor 1");
        System.out.println(li);
        li.setPaginas(100);
        System.out.println(li);
        li.setAutor("Autor 2");
        li.setAutor("Autor 3");
        li.setAutor("Autor 4");
        li.removeAutor("Autor 3");
        System.out.println(li);
        li.setCantidadEjemplares(10);
        System.out.println(li);
        li.prestarEjemplar();
        System.out.println(li);
        Biblioteca bi = new Biblioteca();
        bi.addLibro(li);
        // -- segundo libro
        li = new Libro("0002", "Mi segundo libro");
        System.out.println(li);
        li.setAutor("Autor 1");
        System.out.println(li);
        li.setPaginas(100);
        System.out.println(li);
        li.setAutor("Autor 2");
        li.setAutor("Autor 3");
        li.setCantidadEjemplares(2);
        System.out.println(li);

        bi.addLibro(li);
        System.out.println("Prestados : " + bi.countPrestados());
        li.prestarEjemplar();
        System.out.println("Prestados : " + bi.countPrestados());
        System.out.println("Libros de Autor 1");
        bi.getLibrosAutor("Autor 1");
        System.out.println("Libros de Autor 3");
        bi.getLibrosAutor("Autor 3");
        System.out.println("Libros de Auto");
        bi.getLibrosAutor("auto");
        System.out.println("Buscando Libros con primer");
        bi.hayarLibros("primer");

        // Problema Aliasing para la clase
        Libro pruebaLibro1 = new Libro("un-isbn", "El Problema de los Tres Cuerpos");
        pruebaLibro1.setAutor("Cixin Liu");
        pruebaLibro1.setCantidadEjemplares(10);
        int ejemplaresLibros = pruebaLibro1.getCantidadEjemplares();

        Libro pruebaLibro2 = pruebaLibro1;
        pruebaLibro2.prestarEjemplar();
        pruebaLibro2.prestarEjemplar();
        pruebaLibro2.prestarEjemplar();

        if (ejemplaresLibros != pruebaLibro1.getCantidadEjemplares()) {
            System.out.println("Existe un problema de aliasing");
        } else {
            System.out.println("NO hay un problema de aliasing");
        }


    }
}