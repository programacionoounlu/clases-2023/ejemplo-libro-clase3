package ar.edu.unlu.poo.biblioca.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LibroTest {


    @Test
    void setCantidadEjemplares_en_2_obtengo_2() {
        Libro li = new Libro("1","Libro");
        li.setCantidadEjemplares(2);
        assertEquals(2,li.getCantidadEjemplares());
    }
    @Test
    void setCantidadEjemplares_menores_a_los_prestados() {
        Libro li = new Libro("1","Libro");
        li.setCantidadEjemplares(2);
        li.prestarEjemplar();
        li.setCantidadEjemplares(0);
        assertEquals(2,li.getCantidadEjemplares());
    }
    @Test
    void setCantidadEjemplares_negativo_no_se_puede() {
        Libro li = new Libro("1","Libro");
        li.setCantidadEjemplares(-1);
        assertEquals(0,li.getCantidadEjemplares());
    }

    @Test
    void prestarEjemplar_presta_bien() {
        Libro li = new Libro("1","Libro");
        li.setCantidadEjemplares(10);
        li.prestarEjemplar();

        assertEquals(9,li.getCantidadDisponible());

    }
    @Test
    void prestarEjemplaresCuandoNoHayDaFalso() {
        Libro li = new Libro("1","Libro");
        li.setCantidadEjemplares(1);
        li.prestarEjemplar();
        assertFalse(li.prestarEjemplar());
    }

    @Test
    void agregarAutorYPuedo(){
        Libro li = new Libro("1","Libro");
        li.setCantidadEjemplares(1);

        assertTrue(li.setAutor("Uno"));
        assertTrue(li.esAutor("Uno"));
    }
    @Test
    void agregarAutorYNoEstaOtro(){
        Libro li = new Libro("1","Libro");
        li.setCantidadEjemplares(1);
        assertTrue(li.setAutor("Uno"));
        assertFalse(li.esAutor("Dos"));
    }
    @Test
    void agregarAutorLoRemuevoYNoEsta(){
        Libro li = new Libro("1","Libro");
        li.setCantidadEjemplares(1);
        assertTrue(li.setAutor("Uno"));
        assertTrue(li.removeAutor("Uno"));
        assertFalse(li.esAutor("Uno"));
    }
    @Test
    void agregarAutorNoPuedoMasDeCinco(){
        Libro li = new Libro("1","Libro");
        li.setCantidadEjemplares(1);
        assertTrue(li.setAutor("Uno"));
        assertTrue(li.setAutor("Dos"));
        assertTrue(li.setAutor("Tres"));
        assertTrue(li.setAutor("Cuatro"));
        assertTrue(li.setAutor("Cinco"));
        assertFalse(li.setAutor("NoPuedo"));
    }
    @Test
    void buscarElUltimo(){
        Libro li = new Libro("1","Libro");
        li.setCantidadEjemplares(1);
        assertTrue(li.setAutor("Uno"));
        assertTrue(li.setAutor("Dos"));
        assertTrue(li.setAutor("Tres"));
        assertTrue(li.setAutor("Cuatro"));
        assertTrue(li.setAutor("Cinco"));
        assertTrue(li.esAutor("Cinco"));
    }
    @Test
    void removerElUltimo(){
        Libro li = new Libro("1","Libro");
        li.setCantidadEjemplares(1);
        assertTrue(li.setAutor("Uno"));
        assertTrue(li.setAutor("Dos"));
        assertTrue(li.setAutor("Tres"));
        assertTrue(li.setAutor("Cuatro"));
        assertTrue(li.setAutor("Cinco"));
        assertTrue(li.removeAutor("Cinco"));
        assertFalse(li.esAutor("Cinco"));
    }
    @Test
    void removerContarCuentaSuma(){
        Libro li = new Libro("1","Libro");
        li.setCantidadEjemplares(5);
        li.prestarEjemplar();
        assertEquals(4,li.getCantidadDisponible());
    }

    @Test
    void hallarDevuelveTrue(){
        Libro li = new Libro("1","Libro de las americas");
        li.setCantidadEjemplares(5);
        li.prestarEjemplar();
        assertTrue(li.hallar("libro"));
    }

    @Test
    void hallarDevuelvefalse(){
        Libro li = new Libro("1","Libro de las americas");
        li.setCantidadEjemplares(5);
        li.prestarEjemplar();
        assertFalse(li.hallar("libros"));
    }

}